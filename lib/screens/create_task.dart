import 'package:flutter/material.dart';

class CreateTaskScreen extends StatefulWidget {
  @override
  _CreateTaskScreenState createState() => _CreateTaskScreenState();
}

class _CreateTaskScreenState extends State<CreateTaskScreen> {
  bool remindMe = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text("Titulo"),
        backgroundColor: Colors.transparent,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
                width: MediaQuery.of(context).size.width,
                padding: const EdgeInsets.all(32),
                height: MediaQuery.of(context).size.height * 1.1,
                child: Column(
                  children: [
                    Text(
                      "Create New Task",
                      style: TextStyle(
                          fontSize: 50,
                          height: 1.2,
                          fontWeight: FontWeight.w700,
                          color: Colors.grey[800]),
                    ),

                    // For spacing
                    Spacer(),

                    // Container for TextField
                    TextField(
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w500,
                        ),
                        decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.blueGrey[100])),
                          focusedBorder: UnderlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.blueGrey[100])),
                          hintText: "Task Name",
                          hintStyle: TextStyle(
                              fontSize: 24,
                              height: 1.2,
                              fontWeight: FontWeight.w500,
                              color: Colors.grey[400]),
                        )),

                    // For spacing
                    Spacer(),

                    // Container for timing tray
                    Container(
                      child: Row(
                        children: [
                          /// Container for Icon
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: Color.fromRGBO(255, 240, 240, 1)),
                            padding: const EdgeInsets.all(16),
                            child: Icon(
                              Icons.calendar_today,
                              color: Colors.redAccent,
                            ),
                          ),

                          /// For spacing
                          SizedBox(width: 24),

                          /// For Text
                          Text(
                            "Friday 28, November",
                            style: TextStyle(
                                fontSize: 18,
                                height: 1.2,
                                fontWeight: FontWeight.w700,
                                color: Colors.grey[700]),
                          )
                        ],
                      ),
                    ),

                    // For spacing
                    Spacer(),

                    // Container for timing tray 2
                    Container(
                      child: Row(
                        children: [
                          /// Container for Icon
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: Color.fromRGBO(255, 240, 240, 1)),
                            padding: const EdgeInsets.all(16),
                            child: Icon(
                              Icons.alarm,
                              color: Colors.orangeAccent,
                            ),
                          ),

                          /// For spacing
                          SizedBox(width: 24),

                          /// For Text
                          Text(
                            "1:00 - 3:00 PM",
                            style: TextStyle(
                                fontSize: 18,
                                height: 1.2,
                                fontWeight: FontWeight.w700,
                                color: Colors.grey[700]),
                          )
                        ],
                      ),
                    ),

                    // For spacing
                    Spacer(),

                    // Container for Task Category
                    Container(
                      padding: const EdgeInsets.all(12),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.blueGrey[100],
                        ),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        children: [
                          /// Container for icon
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: Color.fromRGBO(255, 240, 240, 1)),
                            padding: const EdgeInsets.all(16),
                            child: Icon(
                              Icons.web_asset,
                              color: Colors.orangeAccent,
                            ),
                          ),

                          /// For spacing
                          SizedBox(width: 24),

                          /// For Text
                          Text(
                            "Work",
                            style: TextStyle(
                                fontSize: 18,
                                height: 1.2,
                                fontWeight: FontWeight.w700,
                                color: Colors.grey[700]),
                          ),

                          Spacer(),
                          IconButton(
                            icon: Icon(Icons.arrow_forward_ios),
                            onPressed: () {},
                          )
                        ],
                      ),
                    ),

                    /// For spacing
                    Spacer(),

                    // Container for Reminder
                    Container(
                      padding: const EdgeInsets.all(12),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.blueGrey[100],
                        ),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Row(
                        children: [
                          /// Container for icon
                          Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: Color.fromRGBO(240, 235, 255, 1)),
                            padding: const EdgeInsets.all(16),
                            child: Icon(
                              Icons.alarm_on,
                              color: Colors.purpleAccent[100],
                            ),
                          ),

                          /// For spacing
                          SizedBox(width: 24),

                          /// For Text
                          Text(
                            "Remind me",
                            style: TextStyle(
                                fontSize: 18,
                                height: 1.2,
                                fontWeight: FontWeight.w700,
                                color: Colors.grey[700]),
                          ),

                          Spacer(),

                          Switch(
                              value: remindMe,
                              activeColor: Colors.lightBlueAccent,
                              onChanged: (value) {
                                remindMe = value;
                              })
                        ],
                      ),
                    ),

                    Spacer(),

                    // Button for Create Task
                    Container(
                      width: double.infinity,
                      child: FlatButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        padding: const EdgeInsets.symmetric(vertical: 16),
                        color: Colors.black,
                        child: Text(
                          "CREATE TASK",
                          style: TextStyle(
                            fontWeight: FontWeight.w900,
                            fontSize: 18,
                            color: Colors.white,
                          ),
                        ),
                        onPressed: () {},
                      ),
                    )
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
